//
//  WeatherViewController.swift
//  Weather
//
//  Created by Karl Grogan on 01/09/2017.
//  Copyright © 2017 Karl Grogan. All rights reserved.
//

import UIKit
import Charts
import Alamofire
import CoreLocation

class WeatherViewController: UIViewController, ChartViewDelegate {
    var forecastObjects: [NSDictionary] = [NSDictionary]()
    var tempChartView: TemperatureChartView?
    var percipChanceChartView: PercipitationChartView?
    var windChartView: WindChartView?
    var stackView: UIStackView!
    var collectionView: UICollectionView!
    var locationManager = CLLocationManager()
    var city: String!
    var activityView: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Weathr"
        self.view.backgroundColor = UIColor(red: 15/255, green: 22/255, blue: 30/255, alpha: 1)
        
        tempChartView = TemperatureChartView(
            frame: CGRect(x: self.view.frame.minX, y: self.view.frame.minY, width: self.view.frame.width, height: self.view.frame.height / 3))
        tempChartView?.chart.delegate = self
        
        percipChanceChartView = PercipitationChartView(
            frame: CGRect(x: self.view.frame.minX, y: self.view.frame.midY, width: self.view.frame.width, height: self.view.frame.height / 3))
        percipChanceChartView?.chart.delegate = self
        
        windChartView = WindChartView(
            frame: CGRect(x: self.view.frame.minX, y: self.view.frame.midY, width: self.view.frame.width, height: self.view.frame.height / 3))
        windChartView?.chart.delegate = self
      
        stackView = UIStackView(frame: .zero)
        stackView.distribution = .fillEqually
        stackView.alignment = .fill
        stackView.axis = .vertical
        stackView.spacing = 1.0
        stackView.addArrangedSubview(tempChartView!)
        stackView.addArrangedSubview(percipChanceChartView!)
        stackView.addArrangedSubview(windChartView!)
        view.addSubview(stackView)

        setupConstraints()
        
        NotificationCenter.default.addObserver(self, selector: #selector(resumeApp), name: Notification.Name.UIApplicationWillEnterForeground, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("View did appear")
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()        
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager.requestLocation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupConstraints() {
        stackView.translatesAutoresizingMaskIntoConstraints = false
        view.addConstraint(NSLayoutConstraint(item: stackView, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: stackView, attribute: .left, relatedBy: .equal, toItem: view, attribute: .left, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: stackView, attribute: .right, relatedBy: .equal, toItem: view, attribute: .right, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: stackView, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0))
    }
    
    func requestWeatherData(state: String, city: String) {
        // Request the weather information
        print("making request")
        let query = "\(state)/\(city.replacingOccurrences(of: " ", with: "_"))"
        
        Alamofire.request("http://api.wunderground.com/api/1c9b6a9ee5c5ca65/hourly/q/\(query).json").responseJSON { response in
            let json = response.result.value as! NSDictionary
            print(json)
            let forecastArray = json["hourly_forecast"] as? NSArray
            
            if let forecastArray = forecastArray {
                var newLineChartEntries = [ChartDataEntry]()
                var newBarChartEntries = [BarChartDataEntry]()
                var newWindChartEntries = [BarChartDataEntry]()
                
                self.tempChartView?.dataSet.clear()
                for (i, dataObject) in (forecastArray.enumerated()) {
                    let forecastObject = dataObject as! NSDictionary
                    self.forecastObjects.append(forecastObject)
                    
                    let tempObject = forecastObject["temp"] as! NSDictionary
                    let tempDegrees  = tempObject["metric"] as! String
                    
                    let windObject = forecastObject["wspd"] as! NSDictionary
                    let windSpeed  = windObject["metric"] as! String
                    
                    newLineChartEntries.append(ChartDataEntry(x: Double(i), y: Double(tempDegrees)!, data: forecastObject))
                    newBarChartEntries.append(BarChartDataEntry(x: Double(i), y: Double(forecastObject["pop"] as! String)!, data: forecastObject))
                    newWindChartEntries.append(BarChartDataEntry(x: Double(i), y: Double(windSpeed)!, data: forecastObject))
                }
                
                self.tempChartView?.chart.xAxis.valueFormatter = ChartStringFormatter(objects: self.forecastObjects)
                self.percipChanceChartView?.chart.xAxis.valueFormatter = ChartStringFormatter(objects: self.forecastObjects)
                self.windChartView?.chart.xAxis.valueFormatter = ChartStringFormatter(objects: self.forecastObjects)
                
                
                self.tempChartView?.dataSet.values = newLineChartEntries
                self.tempChartView?.chart.leftAxis.spaceTop = CGFloat(40/self.calculateMaxDataEntry(entries: newLineChartEntries))
                self.tempChartView?.chart.data?.notifyDataChanged(); // let the data know a dataSet changed
                self.tempChartView?.chart.notifyDataSetChanged(); // let the chart know it's data changed
                self.tempChartView?.chart.setNeedsDisplay()
                
                self.percipChanceChartView?.dataSet.values = newBarChartEntries
                self.percipChanceChartView?.chart.leftAxis.spaceTop = CGFloat(40/self.calculateMaxDataEntry(entries: newBarChartEntries))
                self.percipChanceChartView?.chart.data?.notifyDataChanged(); // let the data know a dataSet changed
                self.percipChanceChartView?.chart.notifyDataSetChanged(); // let the chart know it's data changed
                self.percipChanceChartView?.chart.setNeedsDisplay()
                
                self.windChartView?.dataSet.values = newWindChartEntries
                self.windChartView?.chart.leftAxis.spaceTop = CGFloat(40/self.calculateMaxDataEntry(entries: newWindChartEntries))
                self.windChartView?.chart.data?.notifyDataChanged(); // let the data know a dataSet changed
                self.windChartView?.chart.notifyDataSetChanged(); // let the chart know it's data changed
                self.windChartView?.chart.setNeedsDisplay()

                print("stopsana")
                self.activityView.stopAnimating()
            }
        }
    }
    
    private func calculateMaxDataEntry(entries: [ChartDataEntry]) -> Double {
        return entries.max(by: { (a, b) -> Bool in
            return a.y < b.y
        })!.y
    }
    
    // Let's me sync the scrolling of the charts
    // https://github.com/danielgindi/Charts/issues/1415
    func chartTranslated(_ chartView: ChartViewBase, dX: CGFloat, dY: CGFloat) {
        if  chartView == tempChartView?.chart {
            let currentMatrix = chartView.viewPortHandler.touchMatrix
            percipChanceChartView?.chart.viewPortHandler.refresh(newMatrix: currentMatrix, chart: (percipChanceChartView?.chart)!, invalidate: true)
            windChartView?.chart.viewPortHandler.refresh(newMatrix: currentMatrix, chart: (windChartView?.chart)!, invalidate: true)
        } else if chartView == percipChanceChartView?.chart {
            let currentMatrix = chartView.viewPortHandler.touchMatrix
            tempChartView?.chart.viewPortHandler.refresh(newMatrix: currentMatrix, chart: (tempChartView?.chart)!, invalidate: true)
            windChartView?.chart.viewPortHandler.refresh(newMatrix: currentMatrix, chart: (windChartView?.chart)!, invalidate: true)
        } else {
            let currentMatrix = chartView.viewPortHandler.touchMatrix
            tempChartView?.chart.viewPortHandler.refresh(newMatrix: currentMatrix, chart: (tempChartView?.chart)!, invalidate: true)
            percipChanceChartView?.chart.viewPortHandler.refresh(newMatrix: currentMatrix, chart: (percipChanceChartView?.chart)!, invalidate: true)
        }
    }

    func resumeApp() {
        print("app resume")
        locationManager.requestLocation()
    }
}

extension WeatherViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("LOCALIZATION ERROR: \(error.localizedDescription)")
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("didupdatelocation")
        let newLocation = locations.last!
        let latitude = newLocation.coordinate.latitude
        let longitude = newLocation.coordinate.longitude

        activityView = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        activityView.center = view.superview!.center
        activityView.startAnimating()

        view.addSubview(activityView)
        
        Alamofire.request("http://api.wunderground.com/api/1c9b6a9ee5c5ca65/geolookup/q/\(latitude),\(longitude).json").responseJSON { response in
            print(response.debugDescription)
            let json = response.result.value as! NSDictionary
            let locationObject = json["location"] as! NSDictionary
            
            let country = locationObject["country"] as! String
            self.city = locationObject["city"] as! String
            self.title = "Weathr - \(self.city!)"
            
            self.locationManager.stopUpdatingLocation()
            self.requestWeatherData(state: country, city: self.city)
        }
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        print("Did change authorization: \(status)")
    }
}

class ChartStringFormatter: NSObject, IAxisValueFormatter {
    var forecastObjects: [NSDictionary] = [NSDictionary]()
    
    init(objects: [NSDictionary]) {
        self.forecastObjects = objects
    }
    
    public func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH"
        if forecastObjects.isEmpty {
            return ""
        }
        if Int(value) == 0 {
            return "Now"
        }
        
        let forecastObject = forecastObjects[Int(value)]
        let timeObject = forecastObject["FCTTIME"] as! NSDictionary
        let timeEpoch = timeObject["epoch"] as! String


        let hour = dateFormatter.string(from: Date(timeIntervalSince1970: Double(timeEpoch)!))

        dateFormatter.dateFormat = "EE"
        let day = dateFormatter.string(from: Date(timeIntervalSince1970: Double(timeEpoch)!))

        return hour == "00" ? day : hour
        
    }
}

